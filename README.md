# create_patch

[ Desc ]
Uses gnu/diff to create a patch file in default .patches folder
[input]
$1 = filename
[opts]
-o --outputfile
-d --dateformat
-u --user
-v --showdiff
